export default {
    methods: {
        formatMoney: function (money) {
            return Number(money).toLocaleString()
        },
        formatMoneyCurrency: function (money) {
            return "Rp "+Number(money).toLocaleString()
        }
    }
}
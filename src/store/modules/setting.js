import database from '../lib/db.js';

function selectData(state, data) {
    var type = data.type;
    var key = data.key;
    var value = data.value;

    return state[type].filter(x => (x.data[key] === value));
}

const state = {
    pegawai: [],
    user: [],
    pembeli: [],
    grup_barang: [],
    settings: [],

    db_list: {
        pegawai: 'pegawai',
        user: 'user',
        pembeli: 'pembeli',
        grup_barang: 'grup_brg',
        settings: 'settings'
    },
};

const getters = {};

const actions = {
    loadData(context, type) {
        database(context.state.db_list[type]).then(result => {
            context.commit("loadData", {data: result, type: type});
        })
    },
    saveData(context, data) {
        // Get data
        var saved = selectData(context.state, data);

        if(!(saved[0][data.template[0]] === "")){
            context.commit("deleteData", data)
        } else {
            var oldKey = saved[0].data[data.key];
            var newKey = saved[0].data[data.template[0]].replace(/\s/, "").toLowerCase();
            // Save old and new key
            if (oldKey !== newKey) {
                // If new item insert
                database(context.state.db_list[data.type]).where(data.key, newKey).then(result => {
                    // Create update list
                    var update = {};
                    update[data.key] = newKey;
                    var keys = Object.keys(saved[0].data);
                    keys.splice(keys.indexOf(data.key), 1);
                    for (var i = 0; i < keys.length; i++) {
                        update[keys[i]] = saved[0].data[keys[i]];
                    }
                    console.log(result)
                    if (result.length === 0) {
                        if (oldKey === "") {
                            console.log("ins")
                            database(context.state.db_list[data.type]).insert(update).then(res => {
                                context.commit("updateKey", {
                                    data: saved[0],
                                    key: data.key,
                                    value: newKey
                                });
                            });
                        } else {
                            console.log("upd")
                            database(context.state.db_list[data.type]).where(data.key, oldKey).update(update).then(result => {
                                context.commit("updateKey", {
                                    data: saved[0],
                                    key: data.key,
                                    value: newKey
                                });
                            });
                        }
                        context.commit("saveData", saved[0]);
                    } else {
                        context.commit('modal/showModal', {
                            title: 'Error',
                            message: 'Kode Grup Barang Sudah Ada!',
                            icon: 'close',
                            type: 'danger'
                        }, {root: true});
                    }
                });
            } else {
                context.commit("saveData", saved[0]);
            }
        }
    },
    deleteData(context, data) {
        database(context.state.db_list[data.type]).where(data.key, data.value).del().then((x) => {
            context.commit("deleteData", data);
        });
    },
    loadSettings(context, data) {
        database("settings").then((res) => {
            context.commit("loadSettings", res)
        })
    },
    saveSettings(context, data) {
        var value = context.state.settings.filter(x => {
            return x.key === data.key
        })[0].value;
        database("settings").where('setting_key', data.key).update('value', value).then((x) => {
            // Success
        });
    }
};

const mutations = {
    editData(state, data) {
        var edited = selectData(state, data);
        if (edited.length === 1) {
            edited[0].edit = true;
        }
    },
    saveData(state, data) {
        data.edit = false;
        data.new = false;
    },
    loadData(state, data) {
        state[data.type] = [];
        data.data.forEach(x => {
            state[data.type].push({edit: false, new: false, data: x})
        });
    },
    updateData(state, data) {
        var updated = selectData(state, data);
        if (updated.length === 1) {
            updated[0].data[data.updated_key] = data.updated_value;
        }
    },
    deleteData(state, data) {
        var deleted = selectData(state, data);
        if (deleted.length === 1) {
            var index = state[data.type].indexOf(deleted[0]);
            state[data.type].splice(index, 1);
        }
    },
    addData(state, data) {
        var state_data = state[data.type];
        var template = {};

        data.template.forEach(x => {
            template[x] = "";
        });
        template[data.key] = "";

        if (state_data.length === 0 || !state_data[state_data.length - 1].new) {
            state_data.push({
                edit: true,
                new: true,
                data: template
            })
        }
    },
    updateKey(state, data) {
        data.data.data[data.key] = data.value;
        console.log(data)
    },
    loadSettings(state, res) {
        state.settings = [];
        res.forEach(x => {
            state.settings.push({key: x.setting_key, value: x.value})
        });
    },
    updateSettings(state, data) {
        var updated = state.settings.filter(x => {
            return x.key === data.key
        });
        updated[0].value = data.value
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
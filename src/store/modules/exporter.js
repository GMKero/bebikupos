import database from '../lib/db.js';

import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

const excel = require('node-excel-export');
var fs = require('fs');

var reportDoc = {
    pageSize: 'A5',
    pageOrientation: 'landscape',
    content: [
        {
            columns: [
                {
                    text: '',
                    fontSize: 11,
                    width: '20%'
                },
                {
                    stack: [
                        {
                            text: '',
                            fontSize: 14,
                            bold: true,
                            underline: true,
                        },
                        {
                            text: '',
                            fontSize: 11,
                        }
                    ],
                    width: '60%',
                    alignment: 'center',
                },
                {
                    text: '',
                    width: '20%',
                    fontSize: 11,
                },
            ]
        },
        "\n\n",
        {
            layout: "headerLineOnly",
            table: {
                widths: [],
                headerRows: 1,
                body: [],
            },
            fontSize: 10,
        }
    ],
};
function generateReportDoc(data){
    var nama_toko = res[0].value;

    // table body
    var tableContent = [];
    var widths = [];

    // Format Date
    const bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    var date = "";
    if (data.config.time === "Per Bulan") {
        date = bulan[data.config.date.getMonth()] + " " + data.config.date.getFullYear();
    } else {
        date = data.config.date.getDate() + " " + bulan[data.config.date.getMonth()] + " " + data.config.date.getFullYear();
    }

    var d = new Date();
    var today = d.getDate() + " " + bulan[d.getMonth()] + " " + d.getFullYear();


    // Group Items
    if (data.config.category === 'Kategori Barang') {
        tableContent = [
            ["Kode Barang", "Nama Barang", "Grup Barang", "Jumlah", "Harga", "Total"]
        ];
        data.data.forEach(x => {
            tableContent.push([x["kode_barang"], x["nama_barang"], x["kode_grup_barang"], x["sum_jumlah_barang"], x["harga"], x["sumtotal"]])
        });
        widths = ["auto", 200, 50, "*", "*", "*"];
    } else {
        tableContent = [
            ["Pegawai", "Kode Barang", "Nama Barang", "Grup Barang", "Jumlah", "Harga", "Total"]
        ];
        data.data.forEach(x => {
            tableContent.push([x["pegawai"], x["kode_barang"], x["nama_barang"], x["kode_grup_barang"], x["sum_jumlah_barang"], x["harga"], x["sumtotal"]])
        });
        widths = ["auto", "auto", 200, 50, "*", "*", "*"];
    }

    var docdef = JSON.parse(JSON.stringify(reportDoc));
    docdef.content[0].columns[0].text = nama_toko;
    docdef.content[0].columns[1].stack[0].text = "\n\nLAPORAN PENJUALAN " + data.config.category.toUpperCase();
    docdef.content[0].columns[1].stack[1].text = date;
    docdef.content[0].columns[2].text = today;
    docdef.content[2].table.widths = widths;
    docdef.content[2].table.body = tableContent;
    return docdef;
}

const state = {};

const getters = {};

const actions = {
    exportDataBarang(context, path) {
        return new Promise((resolve, reject) => {
            if (path) {
                database('stok_brg').join('barang', 'stok_brg.kode_barang', 'barang.kode_barang').then((res) => {
                    var data = excel.buildExport([{
                        name: "Sheet1",
                        specification: {
                            kode_barang: {
                                displayName: 'Kode Barang',
                                width: 100,
                            },
                            nama_barang: {
                                displayName: 'Nama Barang',
                                width: 100,
                            },
                            kode_grup_barang: {
                                displayName: 'Grup Barang',
                                width: 100,
                            },
                            harga: {
                                displayName: 'Harga',
                                width: 100,
                            },
                            warna: {
                                displayName: 'Warna',
                                width: 100,
                            },
                            stok: {
                                displayName: 'Stok',
                                width: 100,
                            },
                        },
                        data: res
                    }]);

                    fs.writeFile(path, data, () => {
                        resolve();
                    });
                });
            }
        });
    },

    exportDataPenjualan(context, path) {
        return new Promise((resolve, reject) => {
            if (path) {
                database('item_jual').join('penjualan', 'penjualan.no_nota', 'item_jual.no_nota').then((res) => {
                    var data = excel.buildExport([{
                        name: "Sheet1",
                        specification: {
                            no_nota: {
                                displayName: 'No Nota',
                                width: 100,
                            },
                            id: {
                                displayName: 'No',
                                width: 100,
                            },
                            kode_barang: {
                                displayName: 'Kode Barang',
                                width: 100,
                            },
                            nama_barang: {
                                displayName: 'Nama Barang',
                                width: 100,
                            },
                            harga: {
                                displayName: 'Harga',
                                width: 100,
                            },
                            warna: {
                                displayName: 'Warna',
                                width: 100,
                            },
                            diskon: {
                                displayName: 'Diskon',
                                width: 100,
                            },
                            jumlah: {
                                displayName: 'Jumlah',
                                width: 100,
                            },
                            total: {
                                displayName: 'Total',
                                width: 100,
                            },
                            notes: {
                                displayName: 'Notes',
                                width: 100,
                            }
                        },
                        data: res
                    }]);

                    fs.writeFile(path, data, () => {
                        resolve();
                    });
                });
            }
        });
    },

    createReport(context, data) {
        database('settings').where('setting_key', 'nama_toko').then(res => {
            pdfMake.createPdf(generateReportDoc(data)).download("LAP_" + data.config.category + "_" + data.config.date.toLocaleDateString().replace("/", "_"));
        });
    },

    previewReport(context, data) {
        database('settings').where('setting_key', 'nama_toko').then(res => {
            pdfMake.createPdf(generateReportDoc(data)).download("LAP_" + data.config.category + "_" + data.config.date.toLocaleDateString().replace("/", "_"));
        });
    }
};

const mutations = {};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
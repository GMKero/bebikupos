import database from '../lib/db.js';

function updateTotal(item){
    item.total = (item.harga * item.jumlah) - item.diskon;
}

const state = {
    transaction: {
        no_nota: '-',
        item_list: [],
        pembeli: '',
        pegawai: '',
        open: true,
        date: '',
    },
    nama_toko: '',
    saved_transaction: [],
    current_loaded: 0,
    pegawai_list: [],
};

const getters = {};

const actions = {
    refreshList(context) {
        // List item bought
        if (context.state.transaction.open) {
            var itemList = [];
            var currentTrx = context.state.transaction;
            currentTrx.item_list.forEach(x => {
                itemList.push(x.kode_barang);
            });

            // Get barang detail & stok and update each item
            database('barang').whereIn('kode_barang', itemList).then(res => {
                database('stok_brg').whereIn('kode_barang', itemList).then(stok_brg => {
                    context.commit('refreshList', {stok: stok_brg, item: res});
                });
            })
        }
    },
    loadTransactions(context) {
        var time = Date.now();
        time = time - ((time % 86400000) + 25200000);

        // Load transactions for today
        database('penjualan').where('tanggal', '>=', time).orderBy('tanggal', 'asc').then((data) => {
            var no_nota = new Set();
            data.forEach((d) => {
                no_nota.add(d.no_nota);
            });

            // Find matching item_jual
            database('item_jual').whereIn('no_nota', Array.from(no_nota)).then((item_data) => {
                var kode_brg = new Set();
                item_data.forEach((d) => {
                    kode_brg.add(d.kode_barang);
                });

                // Find matching stok_brg
                database('stok_brg').whereIn('kode_barang', Array.from(kode_brg)).then(stok_brg => {
                    context.commit('loadTransactions', {transaction: data, item: item_data, stok: stok_brg});
                    context.commit('newJual');
                });
            });

            database('settings').where('setting_key', 'nama_toko').then(res => {
                context.commit('loadNamaToko', res[0].value);
            })
        });
    },
    finishJual(context, data) {
        // Check data

        // Data prep
        var total = context.state.transaction.item_list.reduce((accumulator, current) => {
            accumulator += current.total;
            return accumulator;
        }, 0);

        var tanggal = data.date.getTime();
        var insertedData = {
            tanggal: tanggal,
            pegawai: context.state.transaction.pegawai,
            total: total,
            jumlah_barang: context.state.transaction.item_list.length
        };
        if (context.state.transaction.pembeli) {
            insertedData["pembeli"] = context.state.transaction.pembeli;
        }

        // Transaction
        var no_nota;
        return database.transaction(trx => {
            return trx.insert(insertedData).into('penjualan').then((data) => {
                var insertedRow = [];
                for (var i = 0; i < context.state.transaction.item_list.length; i++) {
                    var rowData = context.state.transaction.item_list[i];
                    insertedRow.push({
                        no_nota: data[0],
                        id: i,
                        kode_barang: rowData.kode_barang,
                        nama_barang: rowData.nama_barang,
                        kode_grup_barang: rowData.kode_grup_barang,
                        warna: rowData.selected_warna,
                        harga: rowData.harga,
                        diskon: rowData.diskon,
                        jumlah: rowData.jumlah,
                        total: rowData.total,
                        notes: rowData.notes
                    });
                }
                no_nota = data[0];

                var promises = [];

                var p = trx.batchInsert('item_jual', insertedRow);
                promises.push(p);
                insertedRow.forEach((r) => {
                    var p = trx('stok_brg')
                        .where('kode_barang', '=', r.kode_barang)
                        .andWhere('warna', '=', r.warna)
                        .decrement('stok', r.jumlah);
                    promises.push(p);
                });

                return Promise.all(promises);
            })
        }).then(() => {
            context.commit('finishJual', {no_nota: no_nota, date: tanggal});
        }).catch((error) => {
            context.commit('modal/showModal', {
                title: 'Gagal Menyimpan Penjualan',
                message: 'Database Error',
                icon: 'close',
                type: 'danger'
            }, {root: true});
        });
    },
    resetJual(context) {
        database.transaction((trx) => {
            var promises = [];
            context.state.transaction.item_list.forEach((d) => {
                var p = trx('stok_brg')
                    .where('kode_barang', '=', d.kode_barang)
                    .andWhere('warna', '=', d.selected_warna)
                    .increment('stok', d.jumlah);
                promises.push(p);
            });

            return Promise.all(promises).then(() => {
                return trx('item_jual').where({no_nota: context.state.transaction.no_nota}).del().then(() => {
                    return trx('penjualan').where({no_nota: context.state.transaction.no_nota}).del()
                });
            })
        }).then(() => {
            context.dispatch('refreshList');
            context.commit('resetJual')
        });
    },
    refreshPegawai(context){
        database('pegawai').then((x) => {
            context.commit("refreshPegawai", x);
        });
    }
};

const mutations = {
    addBarang(state, product) {
        var item = state.transaction.item_list.find(item => item.kode_barang === product.kode_barang);
        if (item) {
            item.jumlah = parseInt(item.jumlah) + 1;
            updateTotal(item);
        } else {
            state.transaction.item_list.push(product);
        }
    },
    deleteBarang(state, product) {
        var index = state.transaction.item_list.findIndex(item => item.kode_barang === product.kode_barang);
        if (index !== -1) {
            state.transaction.item_list.splice(index, 1);
        }
    },
    updateJumlah(state, data) {
        var product = data.product;
        var item = state.transaction.item_list.find(item => item.kode_barang === product.kode_barang);
        if (item) {
            item.jumlah = parseInt(data.jumlah);
            if (data.jumlah === "-") {
                item.jumlah = "-";
            } else if (data.jumlah === "") {
                item.jumlah = "";
            }
            updateTotal(item);
        }
    },
    updateNotes(state, data) {
        var product = data.product;
        var item = state.transaction.item_list.find(item => item.kode_barang === product.kode_barang);
        if (item) {
            item.notes = data.notes;
        }
    },
    updateWarna(state, data) {
        var product = data.product;
        var item = state.transaction.item_list.find(item => item.kode_barang === product.kode_barang);
        if (item) {
            item.selected_warna = data.warna;
        }
    },
    updateDiskon(state, data) {
        var product = data.product;
        var item = state.transaction.item_list.find(item => item.kode_barang === product.kode_barang);
        if (item) {
            if (data.diskon === "-") {
                item.diskon = "-";
            } else if (data.diskon === "") {
                item.diskon = "";
            } else {
                item.diskon = parseFloat(data.diskon);
            }
            updateTotal(item);
        }
    },
    updatePegawai(state, data) {
        state.transaction.pegawai = data;
    },
    toggleDrawer(state, data) {
        var product = data.product;
        var item = state.transaction.item_list.find(item => item.kode_barang === product.kode_barang);
        if (item) {
            item.opened = !item.opened;
        }
    },
    loadTransactions(state, data) {
        state.saved_transaction = [];
        data.transaction.forEach((d) => {
            var filtered_item = data.item.filter(n => n.no_nota === d.no_nota);
            var item_list = [];
            filtered_item.forEach(n => {
                var filtered_stok = data.stok.filter(o => n.kode_barang === o.kode_barang);
                item_list.push({
                    kode_barang: n.kode_barang,
                    nama_barang: n.nama_barang,
                    kode_grup_barang: n.kode_grup_barang,
                    harga: n.harga,
                    jumlah: n.jumlah,
                    diskon: n.diskon,
                    total: n.total,
                    stok_warna: filtered_stok,
                    notes: n.notes,
                    selected_warna: n.warna,
                    opened: false
                })
            });

            state.saved_transaction.push({
                no_nota: d.no_nota,
                item_list: item_list,
                pembeli: d.pembeli,
                pegawai: d.pegawai,
                open: false,
                date: d.tanggal,
            });
        });
    },
    loadNamaToko(state, nama){
        state.nama_toko = nama;
    },
    newJual(state) {
        if (state.saved_transaction.length === 0 || state.saved_transaction[state.saved_transaction.length - 1].item_list.length !== 0) {
            state.transaction = {
                no_nota: '-',
                item_list: [],
                pembeli: '',
                pegawai: '',
                open: true,
                date: '',
            };
            state.saved_transaction.push(state.transaction);
        } else {
            state.transaction = state.saved_transaction[state.saved_transaction.length - 1];
        }
        state.current_loaded = state.saved_transaction.length - 1;
    },
    cancelJual(state) {
        state.saved_transaction.splice(state.current_loaded, 1);
    },
    loadJual(state, data) {
        state.transaction = state.saved_transaction[data];
        state.current_loaded = data;
    },
    finishJual(state, data) {
        state.transaction.open = false;
        state.transaction.date = data.date;
        state.transaction.no_nota = data.no_nota;
    },
    resetJual(state) {
        state.transaction.open = true;
        state.transaction.date = '';
        state.transaction.no_nota = '-';
    },
    refreshList(state, data) {
        var item_data = data.item;
        var stok_data = data.stok;

        var remove_idx = [];
        state.transaction.item_list.forEach((item, idx) => {
            var item_details = item_data.filter(r => r.kode_barang === item.kode_barang);
            var stok_details = stok_data.filter(r => r.kode_barang === item.kode_barang);

            if (item_details.length === 1 && stok_details.length > 0) {
                item.nama_barang = item_details[0].nama_barang;
                item.harga = item_details[0].harga;
                updateTotal(item);
                item.stok_warna = [];
                stok_details.forEach(s => item.stok_warna.push({
                    warna: s.warna,
                    stok: s.stok,
                    lock: true,
                    deleted: false
                }));
            } else {
                remove_idx.push(idx);
            }
        });

        remove_idx.sort((a, b) => (b - a));
        for (var i = remove_idx.length - 1; i >= 0; i--) {
            state.transaction.item_list.splice(remove_idx[i], 1);
        }
    },
    refreshPegawai(state, data){
        state.pegawai_list = data;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
import database from '../lib/db.js';

const state = {
    stock: []
};

const getters = {};

const actions = {
    loadStockData(context) {
        database('settings').where('setting_key', 'min_stock').then((data) => {
            var minstok = parseInt(data[0].value);

            database.select(['kode_barang', 'nama_barang', 'harga'])
                .sum('stok as sumstok')
                .from('stok_brg')
                .joinRaw('natural join barang')
                .groupBy('kode_barang')
                .having('sumstok', '<=', minstok)
                .then((result) => {
                    context.commit('setDataStokMin', result);
                });
        });
    },
    getReportData(context, data) {
        // init
        var start_date = new Date(data.date);
        var end_date = new Date(data.date);

        if (data.time === "Per Bulan") {
            start_date.setHours(0, 0, 0, 0)
            start_date.setDate(1);

            end_date.setHours(23, 59, 59, 999)
            end_date.setMonth(end_date.getMonth() + 1);
            end_date.setDate(0);
        } else {
            start_date.setHours(0, 0, 0, 0)
            end_date.setHours(23, 59, 59, 999)
        }

        return new Promise((resolve, reject) => {
            // get data
            database('penjualan')
                .where('tanggal', '>=', start_date.getTime())
                .andWhere('tanggal', '<=', end_date.getTime()).then(res => {
                // get all trx no, then get all items related
                var trxno = [];
                res.forEach(r => trxno.push(r.no_nota));
                var d = database('item_jual').whereIn('item_jual.no_nota', trxno)
                    .joinRaw("join penjualan on item_jual.no_nota = penjualan.no_nota");

                var group;
                if (data.category === 'Kategori Barang') {
                    group = ['kode_barang', 'nama_barang', 'kode_grup_barang', "harga"];
                    d.select(group)
                        .groupBy(group)
                        .orderBy('kode_grup_barang', "asc")
                        .orderBy('kode_barang', "asc");
                } else {
                    group = ['kode_barang', 'nama_barang', 'kode_grup_barang', 'pegawai', "harga"];
                    d.select(group)
                        .groupBy(group)
                        .orderBy('pegawai', "asc")
                        .orderBy('kode_barang', "asc");
                }

                d.sum('item_jual.total as sumtotal')
                    .sum('jumlah_barang as sum_jumlah_barang');

                d.then((data) => {
                    resolve(data);
                });
            });
        });

    },
};

const mutations = {
    setDataStokMin(state, result) {
        state.stock = result;
    },
    setGraphCategory(state, data) {
        state.graph.category = data;
    },
    setGraphTime(state, data) {
        state.graph.time = data;
    },
    setGraphData(state, data) {
        state.graph.graphData = data;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
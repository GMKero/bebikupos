const state = {
    shown: false,
    title: 'An Error Has Occured',
    message: 'We are unable to save at this moment! Please try again later',
    icon: 'check_circle',
    type: 'danger'
};

const getters = {};

const actions = {};

const mutations = {
    closeModal(state) {
        state.shown = false;
    },
    showModal(state, data) {
        state.shown = true;
        state.title = data.title;
        state.message = data.message;
        state.icon = data.icon;
        state.type = data.type;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
import database from '../lib/db.js';

function validate(data) {
    var konten = data.kode_barang && data.nama_barang && data.harga >= 0;
    var stok = data.stok_warna.length > 0 && data.stok_warna.filter((x) => {
        x.warna.length != 0
    });
    return konten && stok;
}

function cleanData(data) {
    data.kode_barang = data.kode_barang.trim();
    data.nama_barang = data.nama_barang.trim();
    data.harga = isNaN(data.harga) ? 0 : data.harga;

    data.stok_warna.forEach((x) => {
        x.warna = x.warna.trim();
        x.stok = isNaN(x.stok) ? 0 : x.stok;
    });
}

const state = {
    item_list: [],
    grup_barang: [],
};

const getters = {};

const actions = {
    // Barang
    getAllItem(context) {
        // Get first 20 item
        return new Promise((resolve, reject) => {
            database('barang').limit(20).orderBy('kode_barang', 'asc').then(function (data) {
                var kode_barang = [];
                for (var d in data) {
                    kode_barang.push(data[d].kode_barang);
                }
                database('stok_brg').whereIn('kode_barang', kode_barang).orderBy('kode_barang', 'asc').then(function (stock) {
                    context.commit('updateItemList', {item: data, stock: stock});
                    resolve();
                });
            });
        });
    },
    saveItem(context, data) {
        cleanData(data);
        if (validate(data)) {
            return new Promise((resolve, reject) => {
                var kode_change = false;
                if (data.old_kode_barang !== data.kode_barang) {
                    kode_change = true
                }

                // Check if kodebarang exists
                database.from('barang')
                    .where('kode_barang', data.old_kode_barang)
                    .count('kode_barang as count')
                    .then(function (result) {
                        // Save barang description
                        if (result[0].count) {
                            if (kode_change) {
                                reject("Kode barang sudah ada");
                            } else {
                                // Update data
                                database('barang').where('kode_barang', data.old_kode_barang).update({
                                    kode_barang: data.kode_barang,
                                    nama_barang: data.nama_barang,
                                    harga: data.harga,
                                    kode_grup_barang: data.kode_grup_barang,
                                }).then(function () {
                                    (async function () {
                                        // Data filter
                                        var upserted_data = data.stok_warna.filter((data) => data.changed);
                                        var deleted_data = data.stok_warna.filter((data) => data.deleted);

                                        if (deleted_data.length === data.stok_warna.length) {
                                            reject("Stok-warna tidak boleh kosong");
                                        } else {
                                            // Upsert stock
                                            if (upserted_data.length > 0) {
                                                await context.dispatch('upsertStock', {
                                                    data: upserted_data,
                                                    kode_barang: data.kode_barang
                                                });
                                            }

                                            // Delete deleted
                                            if (deleted_data.length > 0) {
                                                await context.dispatch('deleteStock', {
                                                    data: deleted_data,
                                                    kode_barang: data.kode_barang
                                                })
                                            }

                                            context.dispatch('getAllItem').then(() => {
                                                resolve();
                                            });
                                        }
                                    })();
                                })
                            }
                        } else {
                            reject("");
                        }
                    });
            });
        } else {
            reject("Data Barang Tidak Lengkap!");
        }
    },
    newItem(context, data) {
        // Add new item
        return new Promise((resolve, reject) => {
            // Data check
            cleanData(data);
            if (validate(data)) {
                database.from('barang')
                    .where('kode_barang', data.kode_barang)
                    .count('kode_barang as count')
                    .then(function (result) {
                        if (result[0].count) {
                            reject("Kode barang sudah ada");
                        } else {
                            database('barang').insert({
                                kode_barang: data.kode_barang,
                                nama_barang: data.nama_barang,
                                harga: data.harga,
                                kode_grup_barang: data.kode_grup_barang,
                            }).then(function () {
                                var stok_warna = data.stok_warna;
                                context.dispatch('upsertStock', {
                                    data: stok_warna,
                                    kode_barang: data.kode_barang
                                }).then((res) => {
                                    context.dispatch('getAllItem');
                                    resolve();
                                });
                            });
                        }
                    });
            } else {
                reject("Data barang tidak lengkap");
            }
        })
    },
    deleteItem(context, data) {
        return new Promise((resolve, reject) => {
            database('barang').where({kode_barang: data.kode_barang}).del().then((result) => {
                if (result) {
                    context.dispatch('deleteStock', {data: [], kode_barang: data.kode_barang}).then(function () {
                        context.dispatch('getAllItem').then(() => {
                            resolve();
                        });
                    });
                } else {
                    reject();
                }
            });
        });
    },

    // Stock
    upsertStock(context, data) {
        var dtemp = [];
        data.data.forEach((d, idx) => {
            dtemp.push({
                kode_barang : data.kode_barang,
                warna: d.warna,
                stok: d.stok,
            });
        });

        // Upsert data
        var insert = database('stok_brg').insert(dtemp).toString();
        var query = insert + "ON CONFLICT (kode_barang, warna) DO UPDATE SET stok=excluded.stok";
        return database.raw(query);
    },
    deleteStock(context, data) {
        if (data.data.length === 0) {
            return database('stok_brg').where('kode_barang', data.kode_barang).del();
        } else {
            var delArr = [];
            data.data.forEach((d, idx) => {
                delArr.push(d.warna)
            });

            return database('stok_brg').whereIn('warna', delArr).where('kode_barang', data.kode_barang).del();
        }
    },

    // Grup Barang
    getAllGrupBarang(context) {
        database.select().from('grup_brg').then(function (data) {
            context.commit('updateGrupBarang', data);
        })
    },

    // Search
    searchBarang: async function (context, query) {
        if (query.length > 1) {
            var result = await database('barang')
                .whereRaw('LOWER(kode_barang) LIKE ? OR LOWER(nama_barang) LIKE ?', [query + '%', '%' + query + '%'])
                .orderBy('kode_barang', "ASC");

            var kode_barang = [];
            for (var d in result) {
                kode_barang.push(result[d].kode_barang);
            }

            database('stok_brg').whereIn('kode_barang', kode_barang).orderBy('kode_barang', 'asc')
                .then((stockResult) => {
                    console.log(stockResult)
                    context.commit('updateItemList', {item: result, stock: stockResult});
                });
        } else {
            context.dispatch('getAllItem');
        }
    },
};

const mutations = {
    updateItemList(state, data) {
        var item_data = data.item;
        var stock_data = data.stock;
        var idx = 0;
        // Match item and stock
        for (var i in item_data) {
            item_data[i].stok_warna = [];
            if (stock_data.length !== 0) {
                while (idx < stock_data.length && stock_data[idx].kode_barang === item_data[i].kode_barang) {
                    item_data[i].stok_warna.push({
                        warna: stock_data[idx].warna,
                        stok: stock_data[idx].stok,
                        lock: true,
                        deleted: false
                    });
                    idx++;
                }
            }
        }

        state.item_list = item_data;
    },
    updateGrupBarang(state, data) {
        state.grup_barang = data;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
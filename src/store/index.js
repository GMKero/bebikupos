import Vue from 'vue';
import Vuex from 'vuex';

import penjualan from './modules/penjualan.js'
import barang from './modules/barang.js'
import modal from './modules/modal.js'
import dashboard from './modules/dashboard.js'
import setting from './modules/setting.js'
import exporter from './modules/exporter.js'

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        penjualan,
        barang,
        modal,
        dashboard,
        setting,
        exporter
    },
    strict: debug,
})